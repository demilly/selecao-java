package com.desafioIndra.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafioIndra.models.Historico;
import com.desafioIndra.repository.HistoricoRepository;
import com.desafioIndra.service.HistoricoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST Historicos")
@CrossOrigin(origins = "*")
public class HistoricoController {

	@Autowired
	private HistoricoService historicoService;

	@Autowired
	HistoricoRepository historicos;

	@ApiOperation(value = "Importar arquivo CSV")
	@GetMapping(path = "/importarCSV")
	public void setDadosInDB() {
		historicoService.salvarDadosDoHistorico();
	}

	@ApiOperation(value = "Retorna uma lista de historicos")
	@GetMapping("/historicos")
	public List<Historico> listHistoricos() {
		return historicos.findAll();

	}

	@ApiOperation(value = "Retorna unico usuário pelo ID")
	@GetMapping("/historicos/{id}")
	public Optional<Historico> listHistoricobyId(@PathVariable(value = "id") long id) {
		return historicos.findById(id);
	}

	@ApiOperation(value = "Salva um historico")
	@PostMapping("/historicos")
	public Historico postHistorico(@RequestBody @Valid Historico historico) {
		return historicos.save(historico);
	}

	@ApiOperation(value = "Deleta um usuario")
	@DeleteMapping("/historicos")
	public void deleteUser(@RequestBody @Valid Historico historico) {
		historicos.delete(historico);
	}

	@ApiOperation(value = "Atualiza um usuario")
	@PutMapping("/historicos")
	public Historico updateUser(@RequestBody @Valid Historico historico) {
		return historicos.save(historico);
	}

	@ApiOperation(value = "Relatorio Preço Medio por Municipio")
	@GetMapping("/mediaDePrecoDeVendaPorMunicipio")
	public List<?> mediaDePrecoDeVendaPorMunicipio() {
		return historicos.getMediaDePrecoDeVendaPorMunicipio();
	}

	@ApiOperation(value = "Relatorio valor médio do valor da compra e do valor da venda por bandeira")
	@GetMapping("/valorMedioCompraVendaPorBandeira")
	public ResponseEntity<List<?>> valorMedioCompraVendaPorBandeira() {
		List<?> list = historicos.getValorMedioCompraVendaPorBandeira();
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna todas as informacoes agrupadas por regiao")
	@GetMapping(path = "/informacoesImportadasPorSiglaRegiao")
	public ResponseEntity<?> InformacaoImportadasSiglas() {
		List<?> list = historicos.getInformacaoImportadasSiglas();
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation(value = "Retorna todas as informacoes agrupadas por data")
	@GetMapping(path = "/informacoesAgrupadasPorData")
	public ResponseEntity<?> informacoesImportadasPorData() {
		return new ResponseEntity<>(historicos.getInformaçõesAgrupadasPorData(), HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna media de preco de compra e venda por municipios")
	@GetMapping(path = "/mediaDePrecoCompraVendaPorMunicipios")
	public ResponseEntity<?> mediaPrecoCompraVendaPorMunicipio() {
		return new ResponseEntity<>(historicos.getMediaPrecoCompraVendaPorMunicipio(), HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna media de preco de compra por Bandeira")
	@GetMapping(path = "/mediaDePrecoCompraVendaPorBandeira")
	public ResponseEntity<?> mediaPrecoCompraVendaPorBandeira() {
		return new ResponseEntity<>(historicos.getMediaPrecoCompraVendaPorDistribuidora(), HttpStatus.OK);
	}
}
