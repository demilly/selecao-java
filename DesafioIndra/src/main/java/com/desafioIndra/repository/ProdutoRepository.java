package com.desafioIndra.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.desafioIndra.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	
	@Transactional(readOnly=true)
	@Query(value = "select * from produto u where u.produto_nome = :produto limit 1", nativeQuery = true)
	public Optional<Produto> buscarPorProduto(@Param("produto") String produto);

	@Transactional(readOnly=true)
	@Query(value = "select count(*) from produto", nativeQuery = true)
	public Integer contaTamanho();	

}

