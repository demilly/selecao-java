package com.desafioIndra.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.desafioIndra.models.Historico;

@Repository
public interface HistoricoRepository extends JpaRepository<Historico, Long> {

	// Retorna consulta Venda Media agrupada por Municipio
	@Query(nativeQuery = true, value = "SELECT loc.MUNICIPIO, round(avg(his.VALOR_VENDA) , 2) as \"VALOR_VENDA\"  \n "
			+ "FROM REVENDEDOR rev \n" 
			+ "INNER JOIN LOCALIZACAO loc ON loc.LOCALIZACAO_ID = rev.LOCALIZACAO_ID \n"
			+ "JOIN HISTORICO his ON his.REVENDEDOR_ID = rev.ID \n"
			+ "GROUP BY loc.MUNICIPIO")
	List<?> getMediaDePrecoDeVendaPorMunicipio();

	// Retorna o valor médio do valor da compra e do valor da venda por bandeira
	@Query(nativeQuery = true, value = "SELECT rev.BANDEIRA, round(avg(hc.VALOR_COMPRA), 2) as \"VALOR_COMPRA\",  \n"
			+ " round(avg(hc.VALOR_VENDA), 2) as \"VALOR_VENDA\"  \n"
			+ "FROM HISTORICO hc JOIN REVENDEDOR rev ON rev.ID = hc.REVENDEDOR_ID GROUP BY rev.BANDEIRA")
	List<?> getValorMedioCompraVendaPorBandeira();

	// Retorna todas as informações importadas por sigla da região
	@Query(nativeQuery = true, value = "SELECT loc.REGIAO, loc.UF, loc.MUNICIPIO, rev.REVENDEDOR_NOME, p.CODIGO, \n"
			+ "p.PRODUTO_NOME,  h.DATA_COLETA, h.VALOR_COMPRA, h.VALOR_VENDA, p.UNIDADE_MEDIDA, rev.BANDEIRA \n"
			+ "FROM HISTORICO h \n" + "JOIN REVENDEDOR rev on h.REVENDEDOR_ID = rev.ID \n"
			+ "JOIN LOCALIZACAO loc on rev.LOCALIZACAO_ID = loc.LOCALIZACAO_ID \n"
			+ "JOIN PRODUTO p on h.PRODUTO_ID = p.ID \n"
			+ "GROUP BY loc.REGIAO, h.DATA_COLETA, rev.REVENDEDOR_NOME, h.VALOR_VENDA")
	List<?> getInformacaoImportadasSiglas();

	// Retorna os dados agrupados por distribuidora
	@Query(nativeQuery = true, value = "SELECT rev.BANDEIRA, loc.UF, loc.MUNICIPIO, rev.REVENDEDOR_NOME, p.CODIGO, \n"
			+ "p.PRODUTO_NOME, h.DATA_COLETA, h.VALOR_COMPRA, h.VALOR_VENDA, p.UNIDADE_MEDIDA, loc.REGIAO \n"
			+ "FROM HISTORICO h \n" + "JOIN REVENDEDOR rev on h.REVENDEDOR_ID = rev.ID \n"
			+ "JOIN LOCALIZACAO loc on rev.LOCALIZACAO_ID = loc.LOCALIZACAO_ID \n"
			+ "JOIN PRODUTO p on h.PRODUTO_ID = p.ID \n"
			+ "GROUP BY rev.BANDEIRA , rev.REVENDEDOR_NOME, h.VALOR_VENDA, H.DATA_COLETA")
	List<?> getInformaçõesImportadasPorDistribuidora();

	// Retorna os dados agrupados por data
	@Query(nativeQuery = true, value = "SELECT h.DATA_COLETA, loc.REGIAO, loc.UF, loc.MUNICIPIO, rev.REVENDEDOR_NOME, \n"
			+ "p.CODIGO, p.PRODUTO_NOME,  h.VALOR_COMPRA, h.VALOR_VENDA, p.UNIDADE_MEDIDA, rev.BANDEIRA \n"
			+ "FROM HISTORICO h \n" + "JOIN REVENDEDOR rev on h.REVENDEDOR_ID = rev.ID \n"
			+ "JOIN LOCALIZACAO loc on rev.LOCALIZACAO_ID = loc.LOCALIZACAO_ID \n"
			+ "JOIN PRODUTO p on h.PRODUTO_ID = p.ID \n"
			+ "GROUP BY H.DATA_COLETA, rev.BANDEIRA , rev.REVENDEDOR_NOME, h.VALOR_VENDA")
	List<?> getInformaçõesAgrupadasPorData();
	
	// Retorna o valor médio do valor da compra e do valor da venda por município
	@Query(nativeQuery = true, value = "SELECT loc.MUNICIPIO, round(avg(his.VALOR_COMPRA) , 2) as \"VALOR_COMPRA\",  \n"
			+ "round(avg(his.VALOR_VENDA) , 2)  as \"VALOR_VENDA\"  \n"
			+ "FROM REVENDEDOR rev \n"  
			+ "INNER JOIN LOCALIZACAO loc ON loc.LOCALIZACAO_ID = rev.LOCALIZACAO_ID \n"
			+ "JOIN HISTORICO his ON his.REVENDEDOR_ID = rev.ID \n"
			+ "GROUP BY loc.MUNICIPIO")
	List<?> getMediaPrecoCompraVendaPorMunicipio();
	
	// Retorna o valor médio do valor da compra e do valor da venda por bandeira
	@Query(nativeQuery = true, value = "SELECT rev.BANDEIRA, round(avg(his.VALOR_COMPRA) , 2) as \"VALOR_COMPRA\", \n"
			+ "round(avg(his.VALOR_VENDA) , 2)  as \"VALOR_VENDA\" \n"
			+ " FROM REVENDEDOR rev \n" 
			+ "INNER JOIN LOCALIZACAO loc ON loc.LOCALIZACAO_ID = rev.LOCALIZACAO_ID \n"
			+ "JOIN HISTORICO his ON his.REVENDEDOR_ID = rev.ID \n"
			+ "GROUP BY rev.BANDEIRA")
	List<?> getMediaPrecoCompraVendaPorDistribuidora();
	
	
}
