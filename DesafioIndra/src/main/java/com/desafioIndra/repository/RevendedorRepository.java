package com.desafioIndra.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.desafioIndra.models.Revendedor;

public interface RevendedorRepository extends JpaRepository<Revendedor, Long> {

	
	@Transactional(readOnly=true)
	@Query(value = "select * from revendedor u where u.revendedor_nome = :revendedor limit 1", nativeQuery = true)
	public Optional<Revendedor> buscarPorRevendedor(@Param("revendedor") String revendedor);
	
}


