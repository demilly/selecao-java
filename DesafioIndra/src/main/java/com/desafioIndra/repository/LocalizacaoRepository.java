package com.desafioIndra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.desafioIndra.models.Localizacao;

public interface LocalizacaoRepository extends JpaRepository<Localizacao, Long> {

	@Transactional(readOnly = true)
	@Query(value = "select * from localizacao u where u.municipio = :municipio limit 1", nativeQuery = true)
	public Optional<Localizacao> buscarPorMunicipio(@Param("municipio") String municipio);

	@Transactional(readOnly = true)
	@Query(value = "select count(*) from localizacao", nativeQuery = true)
	public Integer contaTamanho();
	
}
