package com.desafioIndra.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafioIndra.models.Localizacao;
import com.desafioIndra.repository.LocalizacaoRepository;

@Service
public class LocalizacaoService {

	@Autowired
	private LocalizacaoRepository lr;

	public Localizacao buscaPorMunicipio(String municipio) {

		Optional<Localizacao> obj = lr.buscarPorMunicipio(municipio);
		if (!obj.isPresent()) {
			return null;
		}
		return obj.get();
	}

	public Integer contaRegistro() {

		Integer valor = lr.contaTamanho();

		return valor;
	}

}
