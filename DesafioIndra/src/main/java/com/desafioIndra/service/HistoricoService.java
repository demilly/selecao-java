package com.desafioIndra.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafioIndra.models.Historico;
import com.desafioIndra.models.Localizacao;
import com.desafioIndra.models.Produto;
import com.desafioIndra.models.Revendedor;
import com.desafioIndra.repository.HistoricoRepository;
import com.desafioIndra.repository.LocalizacaoRepository;
import com.desafioIndra.repository.ProdutoRepository;
import com.desafioIndra.repository.RevendedorRepository;

@Service
public class HistoricoService {

	@Autowired
	private LocalizacaoService locService;

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private RevendedorService revendedorService;

	@Autowired
	private LocalizacaoRepository lr;

	@Autowired
	private RevendedorRepository rr;

	@Autowired
	private ProdutoRepository pr;

	@Autowired
	private HistoricoRepository hr;

	public void salvarDadosDoHistorico() {

		String linha = "";
		Integer tamanhoBancoLocalizacao;

		tamanhoBancoLocalizacao = locService.contaRegistro();

		try {

			BufferedReader br = new BufferedReader(new FileReader("src/main/resources/2018-1_CA.csv"));// 2018-1_CA.csv
			br.readLine();
			while ((linha = br.readLine()) != null) {
				linha = linha.replaceAll("S M D  DOS", "S M D DOS");
				linha = linha.replaceAll("  LTDA", " LTDA");
				linha = linha.replaceAll("  MENDONCA", " MENDONCA");
				linha = linha.replaceAll("  - ME", " - ME");
				linha = linha.replaceAll("  DERIVADO", " DEDERIVADO");
				linha = linha.replaceFirst(" DECIO", "  DECIO");
				linha = linha.replaceAll("  VIANA", " VIANA");
				linha = linha.replaceAll("TAREM ", "TAREM  ");
				linha = linha.replaceAll("  &", " &");
				linha = linha.replaceAll("DERIVADOS  DE", "DERIVADOS DE");
				linha = linha.replaceAll("SANTAREM  LTDA", "SANTAREM LTDA");
				linha = linha.replaceAll("FREITAS DEDERIVADOS", "FREITAS  DEDERIVADOS");
				linha = linha.replaceAll("ITABUNA ", "ITABUNA  ");
				linha = linha.replaceAll("SOUSA DEDERIVADOS", "SOUSA  DEDERIVADOS");
				linha = linha.replaceAll("CRISTINA  SOUSA", "CRISTINA SOUSA");
				linha = linha.replaceAll("LONDRINA DEDERIVADOS", "LONDRINA  DEDERIVADOS");
				linha = linha.replaceAll("  ENERGY", " ENERGY");
				linha = linha.replaceAll("REDE  RETA", "REDE RETA");
				linha = linha.replaceAll("PARAISO  JACAR?", "PARAISO JACAR?");
				linha = linha.replaceAll("OTONI DEDERIVADOS", "OTONI  DEDERIVADOS");
				linha = linha.replaceAll(" POSTO  DE  ABASTECIMENTO  AMOR  DA  ILHA LTDA",
						" POSTO DE ABASTECIMENTO AMOR DA ILHA LTDA");
				linha = linha.replaceAll("GASOLINA  PRESIDENTE", "GASOLINA PRESIDENTE");
				linha = linha.replaceAll("LUAR  DE", "LUAR DE");
				linha = linha.replaceAll("ARARAS DEDERIVADOS", "ARARAS  DEDERIVADOS");
				linha = linha.replaceAll("BURAC?O  DE ASSIS", "BURAC?O DE ASSIS");
				linha = linha.replaceAll("ROFEC AUTO  POSTO", "ROFEC AUTO POSTO");
				linha = linha.replaceAll("GUARATINGUET? LTDA  EPP ", "GUARATINGUET? LTDA EPP ");
				linha = linha.replaceAll("GERAIS DE  LINS", "GERAIS DE LINS");
				linha = linha.replaceAll("PRODUTOS DE  PETR?LEO", "PRODUTOS DE PETR?LEO");
				linha = linha.replaceAll("PETROVIA  EIRELI", "PETROVIA EIRELI");
				linha = linha.replaceAll("ITAMARATI  JAYA", "ITAMARATI JAYA");
				linha = linha.replaceAll("POSTO  92", "POSTO 92");
				linha = linha.replaceAll("5746  ETANOL", "5746 ETANOL");
				linha = linha.replaceAll("ILHEUS DEDERIVAD", "ILHEUS  DEDERIVAD");
				linha = linha.replaceAll("COM�RCIO  DE COMBUST�VEIS", "COM�RCIO DE COMBUST�VEIS");
				linha = linha.replaceAll("CCS  COMERCIO", "CCS COMERCIO");
				linha = linha.replaceAll("MORRO DO  OURO", "MORRO DO OURO");
				linha = linha.replaceAll("POSTO  MORRO", "POSTO MORRO");
				linha = linha.replaceAll("DERIV. DE  PETR", "DERIV. DE PETR");
				linha = linha.replaceAll("J.B.  COMERCIO", "J.B. COMERCIO");
				linha = linha.replaceAll("MACK II  COMERCIO", "MACK II COMERCIO");
				linha = linha.replaceAll("IGUATU MENDONCA", "IGUATU  MENDONCA");
				linha = linha.replaceAll("COMBUSTIVEIS  JAGUARIBE", "COMBUSTIVEIS JAGUARIBE");
				linha = linha.replaceAll("RECIFE ENERGY", "RECIFE  ENERGY");
				linha = linha.replaceAll("VELHA DEDERIVADOS", "VELHA  DEDERIVADOS");
				linha = linha.replaceAll("                                                                          PE",
						"  ");

				String[] dados = linha.split("  ");

				Localizacao loc = new Localizacao();
				if (tamanhoBancoLocalizacao == 0) {

					loc.setRegiao(dados[0]);
					loc.setUf(dados[1]);
					loc.setMunicipio(dados[2]);
					lr.save(loc);
					tamanhoBancoLocalizacao = 1;

				} else {

					Localizacao existe = locService.buscaPorMunicipio(dados[2]);
					if (existe == null || !existe.getMunicipio().equals(dados[2])) {

						loc.setRegiao(dados[0]);
						loc.setUf(dados[1]);
						loc.setMunicipio(dados[2]);
						lr.save(loc);
					}
				}

				Produto pd = new Produto();
				pd.setProdutoNome(dados[5]);
				pd.setCodigo(Long.parseLong(dados[4]));
				pd.setUnidadeMedida(dados[9]);
				pr.save(pd);

				SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
				Revendedor rev = new Revendedor();
				rev.setRevendedorNome(dados[3]);
				rev.setBandeira(dados[10]);

				// Adicionado Busca
				rev.setLocalizacao(locService.buscaPorMunicipio(dados[2]));
				rr.save(rev);

				Historico h = new Historico();
				h.setDataColeta(formato.parse(dados[6]));
				if (("".equals(dados[8]) || null == dados[8])) {
					h.setValorVenda(0.0);
				} else {
					dados[8] = dados[8].replace(',', '.');
					h.setValorVenda(Double.parseDouble(dados[8]));
				}
				if (("".equals(dados[7]) || null == dados[7])) {
					h.setValorCompra(0.0);
				} else {
					dados[7] = dados[7].replace(',', '.');
					h.setValorCompra(Double.parseDouble(dados[7]));
				}

				h.setRevendedor(revendedorService.buscaPorRevendedor(dados[3]));
				h.setProduto(produtoService.buscaPorProduto(dados[5]));
				hr.save(h);

			}

			br.close();
		} catch (

		Exception e) {
			System.out.println("Erro: " + e);
			System.out.println(linha);
		}
	}

}
