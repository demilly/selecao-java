package com.desafioIndra.service;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafioIndra.models.Revendedor;
import com.desafioIndra.repository.RevendedorRepository;

@Service
public class RevendedorService {

	@Autowired
	private RevendedorRepository prepo;

	public Revendedor buscaPorRevendedor(String revendedor) {

		Optional<Revendedor> obj = prepo.buscarPorRevendedor(revendedor);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Revendedor: " + revendedor + ", Tipo: " + Revendedor.class.getName(),
				revendedor));
	}

	
}
