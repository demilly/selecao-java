package com.desafioIndra.service;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafioIndra.models.Produto;
import com.desafioIndra.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository prepo;

	public Produto buscaPorProduto(String produto) {

		Optional<Produto> obj = prepo.buscarPorProduto(produto);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Produto: " + produto + ", Tipo: " + Produto.class.getName(), produto));
	}

	public Integer contaRegistro() {

		Integer valor = prepo.contaTamanho();

		return valor;
	}

}
